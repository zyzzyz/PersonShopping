package com.qlw.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qlw.bean.Bill;
import com.qlw.bean.Menu;
import com.qlw.service.BillService;
import com.qlw.service.impl.BillServiceImpl;
import com.qlw.util.Page;

public class BillServlet extends HttpServlet {

	
	private BillService billService = new BillServiceImpl();
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request,response);

	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

     String method = request.getParameter("method");
		
		
		if(method.equals("find")){
			findBybid(request,response);
		}
		
		
		if(method.equals("delete")){
			delete(request,response);
		}
		if(method.equals("page")){
			findPage(request,response);
			
		}   
		
	}
		public void findAll(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
			try{
				List<Bill> listBill =billService.findAll();
				
				request.setAttribute("listBill", listBill);
				request.getRequestDispatcher("/yemian/bill.jsp").forward(request, response);
				
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		// 根据bid 查找菜单
	private void findBybid(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
			
			String bid = request.getParameter("id");
			
			Bill bill = billService.findBybid(bid);
			
			request.setAttribute("bill", bill);
			request.getRequestDispatcher("/yemian/bill.jsp").forward(request, response);
		}

	//删除菜单
	public void delete(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		
		String bid = request.getParameter("id");
		
		billService.delete(bid);
		//findAll( request, response);
		findPage(request, response);
		//request.getRequestDispatcher("/yemian/bill.jsp").forward(request, response);
	}

	public void findPage(HttpServletRequest request,HttpServletResponse response) 
			throws ServletException, IOException{
			
			try{
			
			String currentPage=request.getParameter("pageNo") ;
			
			
			int currentPageNo = 1;
			
			if(currentPage != null){
				currentPageNo = Integer.parseInt(currentPage) ;
			}
				Page<Bill> bill=billService.findPage(currentPageNo);
				
				request.setAttribute("listBill",bill);
				request.getRequestDispatcher("/yemian/bill.jsp").forward(request, response);
		}catch (Exception e) {
			e.printStackTrace();
		}

		}
	}


	

