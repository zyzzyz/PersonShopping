package com.qlw.servlet;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qlw.bean.Cart;
import com.qlw.bean.Menu;
import com.qlw.dao.cartDao;
import com.qlw.service.BillService;
import com.qlw.service.impl.BillServiceImpl;



public class CartServlet extends HttpServlet {

	private String action ; //表示购物车的动作 ,add,show,delete
	//商品业务逻辑类的对象
	private cartDao idao = new cartDao();
	
	private BillService billService= new BillServiceImpl();

	public CartServlet() {
		super();
	}

	public void destroy() {
		super.destroy(); 
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
        doPost(request,response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//response.setContentType("text/html;charset=utf-8");
		
		if(request.getParameter("action")!=null)
		{
			this.action = request.getParameter("action");
			
			if(action.equals("bill")){
				if( bill(request,response)){
				request.getRequestDispatcher("/yemian/fail.jsp").forward(request, response);
				}
				}
			
			
			if(action.equals("add")) //如果是添加商品进购物车
			{
				if(addToCart(request,response))
				{
					request.getRequestDispatcher("/yemian/cart.jsp").forward(request, response);
				}
				else
				{
					request.getRequestDispatcher("/login.jsp").forward(request, response);
				}
			}
			if(action.equals("show"))//如果是显示购物车
			{
				request.getRequestDispatcher("/yemian/cart.jsp").forward(request, response);
			}
			if(action.equals("delete")) //如果是执行删除购物车中的商品
			{
				if(deleteFromCart(request,response))
				{
					request.getRequestDispatcher("/yemian/cart.jsp").forward(request, response);
				}
				else
				{
					
					request.getRequestDispatcher("/yemian/fail.jsp").forward(request, response);
				}
			}
			
			if(action.equals("empty"))
			{
				empty(request,response);
				request.getRequestDispatcher("/yemian/success.jsp").forward(request, response);
			}
			
			if(action.equals("empty1"))
			{
				empty(request,response);
				request.getRequestDispatcher("/yemian/cart.jsp").forward(request, response);
			}
		}
		
	}

	//添加商品进购物车的方法
	private boolean addToCart(HttpServletRequest request, HttpServletResponse response)
	{
		String id = request.getParameter("id");
		String number= request.getParameter("num");
	
		Menu item = idao.getMenuById(id);
		
		//是否是第一次给购物车添加商品,需要给session中创建一个新的购物车对象
		if(request.getSession().getAttribute("cart")==null)
		{
			Cart cart = new Cart();
			request.getSession().setAttribute("cart",cart);
		}
		Cart cart = (Cart)request.getSession().getAttribute("cart");
		if(cart.addGoodsInCart(item, Integer.parseInt(number)))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	//从购物车中删除商品
	private boolean deleteFromCart(HttpServletRequest request, HttpServletResponse response)
	{
		String id = request.getParameter("id");
		Cart cart = (Cart)request.getSession().getAttribute("cart");
		Menu item = idao.getMenuById(id);
		System.out.println(111111);
		System.out.println(item.getMname());
	    if(cart.removeGoodsFromCart(item))
	    {
	    	
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	//账单信息
	private boolean bill(HttpServletRequest request, HttpServletResponse response){
		Cart cart = (Cart)request.getSession().getAttribute("cart");
		Float t=(float) cart.getTotalPrice();
		String m= request.getParameter("money");
		Float money =Float.parseFloat(m);//实收金额
		Float zero =money-t; //找零
		billService.addBill(cart);
		request.setAttribute("zero", zero);
		request.setAttribute("money", money );
		return true;
	}
	public void init() throws ServletException {
		// Put your code here
	}
	
	private void empty (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
	
		request.getSession().invalidate();
		//request.getRequestDispatcher("/yemian/success.jsp").forward(request, response);
	}
}
