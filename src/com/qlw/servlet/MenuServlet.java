package com.qlw.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qlw.bean.Menu;
import com.qlw.service.MenuService;
import com.qlw.service.impl.MenuServiceImpl;
import com.qlw.util.Page;

public class MenuServlet extends HttpServlet {

	private MenuService menuService = new MenuServiceImpl();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request,response);
       
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String method = request.getParameter("method");
		
		if(method.equals("add")){
		
			addMenu(request,response);
		}
		
		if(method.equals("find")){
			findByid(request,response);
			request.getRequestDispatcher("/yemian/updatemenu.jsp").forward(request, response);
		}
		
		if(method.equals("find1")){
			findByid(request,response);
			request.getRequestDispatcher("/yemian/detail.jsp").forward(request, response);
		}
		
		if(method.equals("update")){
			updateMenu(request,response);
		}
		
		if(method.equals("delete")){
			delete(request,response);
		}
		if(method.equals("page")){
			findPage(request,response);
			request.getRequestDispatcher("/yemian/menumanagement.jsp").forward(request, response);
		}
        
		if(method.equals("page1")){
			findPage(request,response);
			request.getRequestDispatcher("/yemian/menu.jsp").forward(request, response);
		}
		
		

	}

	private void addMenu(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String mname = request.getParameter("mname");
		String Mprice =request.getParameter("mprice");
		Float mprice =Float.parseFloat(Mprice);
		
		if(menuService.findBymname(mname)){
			
			request.getRequestDispatcher("/yemian/addmenufalse.jsp").forward(request, response);
		}else{
		
		Menu menu = new Menu();
		menu.setMname(mname);
		menu.setMprice(mprice);
		menuService.addMenu(menu);
		
		findPage(request, response);
		request.getRequestDispatcher("/yemian/menumanagement.jsp").forward(request, response);
		}
		
		}

	//查找全部菜单
	public void findAll(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		try{
			List<Menu> listmenu = menuService.findAll();
			
			request.setAttribute("listmenu", listmenu);
			request.getRequestDispatcher("/yemian/menumanagement.jsp").forward(request, response);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	// 根据mid 查找菜单
private void findByid(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		
		String id = request.getParameter("id");
		
		Menu menu = menuService.findByid(id);
		
		request.setAttribute("menu", menu);
		//request.getRequestDispatcher("/yemian/updatemenu.jsp").forward(request, response);
	}

//删除菜单
public void delete(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
	
	String id = request.getParameter("id");
	
	menuService.delete(id);
	//findAll( request, response);
	findPage(request, response);
	request.getRequestDispatcher("/yemian/menumanagement.jsp").forward(request, response);
}

//更新菜单
public void updateMenu(HttpServletRequest request,HttpServletResponse response)
		throws ServletException, IOException{
		
			String id = request.getParameter("id");
			String mname = request.getParameter("mname");
			String Mprice = request.getParameter("mprice");
			Float mprice =Float.parseFloat(Mprice);
			
			Menu menu = new Menu ();
			menu.setId(Integer.parseInt(id));
			menu.setMname(mname);
			
			menu.setMprice(mprice);
			
			menuService.updateMenu(menu);
			
			//findAll( request, response);查找全部用户
			findPage(request, response);
			request.getRequestDispatcher("/yemian/menumanagement.jsp").forward(request, response);
	};
	public void findPage(HttpServletRequest request,HttpServletResponse response) 
			throws ServletException, IOException{
			
			try{
			
			String currentPage=request.getParameter("pageNo") ;
			
			
			int currentPageNo = 1;
			
			if(currentPage != null){
				currentPageNo = Integer.parseInt(currentPage) ;
			}
				Page<Menu> menu= menuService.findPage(currentPageNo);
				
				request.setAttribute("listmenu", menu);
				//request.getRequestDispatcher("/yemian/listmenu.jsp").forward(request, response);
		}catch (Exception e) {
			e.printStackTrace();
		}

		}
	
	}

