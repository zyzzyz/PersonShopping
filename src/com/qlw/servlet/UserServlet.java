package com.qlw.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qlw.service.UserService;
import com.qlw.service.impl.UserServiceImpl;
import com.qlw.bean.User;
import com.qlw.util.Page;

public class UserServlet extends HttpServlet {

	public UserService userService = new UserServiceImpl();
 	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request,response);
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
String method = request.getParameter("method");
		
		if(method.equals("add")){
			addUser(request,response);
		}
		
		if(method.equals("find")){
			findByid(request,response);
		}
		if(method.equals("update")){
			updateUser(request,response);
		}
		
		if(method.equals("delete")){
			delete(request,response);
		}
		if(method.equals("page")){
			findPage(request,response);
		}

	}
	
	

	/**
	 * 添加用户
	 * @param userName
	 * @param passWord
	 * @throws IOException 
	 * @throws ServletException 
	 */
	public void addUser(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		
		String username = request.getParameter("userName");
		String password = request.getParameter("password");
		
		
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		userService.addUser(user);
		
		
		request.getRequestDispatcher("/all.jsp").forward(request, response);
	}
	
	/**
	 * 查找全部的信息列表
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void findAll(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		try{
			List<User> listuser = userService.findAll();
			
			request.setAttribute("listuser", listuser);
			request.getRequestDispatcher("/yemian/usermanagement.jsp").forward(request, response);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * 根据id来查找用户
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void findByid(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		
		String id = request.getParameter("id");
		
		User user = userService.findByid(id);
		
		request.setAttribute("user", user);
		request.getRequestDispatcher("/yemian/updateuser.jsp").forward(request, response);
	}
	
	public void updateUser(HttpServletRequest request,HttpServletResponse response)
		throws ServletException, IOException{
		
			String id = request.getParameter("userid");
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			
			User user = new User();
			user.setId(Integer.parseInt(id));
			user.setUsername(username);
			user.setPassword(password);
			
			userService.updateUser(user);
			
			//findAll( request, response);查找全部用户
			findPage(request, response);
			
	};
	
	/**
	 * 删除用户信息
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws ServletException 
	 */
	public void delete(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		
		String id = request.getParameter("id");
		
		userService.delete(id);
		//findAll( request, response);
		findPage(request, response);
	}
	
	
	public void findPage(HttpServletRequest request,HttpServletResponse response) 
		throws ServletException, IOException{
		
		try{
		
//		String currentPage=request.getParameter("pageNo") ;
		String	currentPage = "1";
		
		int currentPageNo = 1;
		
		if(currentPage != null){
			currentPageNo = Integer.parseInt(currentPage) ;
		}
			Page<User> pages = userService.findPage(currentPageNo);
			
			request.setAttribute("listuser", pages);
			request.getRequestDispatcher("/yemian/usermanagement.jsp").forward(request, response);
	}catch (Exception e) {
		e.printStackTrace();
	}

	}
}
