package com.qlw.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.qlw.service.UserService;
import com.qlw.service.impl.UserServiceImpl;

public class LoginServlet extends HttpServlet {

	private UserService userService = new UserServiceImpl();
	
	public void doGet(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{
			doPost(request,response);
		}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		   String userName = request.getParameter("userName");//获取页面传到后台的用户名的值
		   String passWord = request.getParameter("password");//获取页面传到后台的密码的值
		   
		   //检测数据库是否有记录
		   boolean check = userService.CheckUserAndPasswd(userName, passWord);
			if(check)
			{
				String msg = "登录成功";
				
				request.setAttribute("msg", msg);
				
				request.getRequestDispatcher("/all.jsp").forward(request,response);
			}else{
				String msg = "用户名或密码错误";
				request.setAttribute("mag",msg);
				request.getRequestDispatcher("/login.jsp").forward(request,response);
			}
			}
}

