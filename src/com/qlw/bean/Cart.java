package com.qlw.bean;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

//���ﳵ��
public class Cart {

	//������Ʒ�ļ���
	private HashMap<Menu,Integer> goods;
	
	
	
	//���ﳵ���ܽ��
	private double totalPrice;

	//���췽��
	public Cart()
	{
		goods = new HashMap<Menu,Integer>();
		totalPrice = 0.0;
	}
	
	
	public HashMap<Menu, Integer> getGoods() {
		return goods;
	}

	public void setGoods(HashMap<Menu, Integer> goods) {
		this.goods = goods;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	//�����Ʒ���ﳵ�ķ���
	public boolean addGoodsInCart(Menu item ,int number)
	{
		if(goods.containsKey(item))
		{
			goods.put(item, goods.get(item)+number);
		}
		else
		{
			goods.put(item, number);	
		}
		calTotalPrice(); //���¼��㹺�ﳵ���ܽ��
		return true;
	}
	
	//ɾ����Ʒ�ķ���
	public boolean removeGoodsFromCart(Menu item)
	{
		goods.remove(item);
		calTotalPrice(); //���¼��㹺�ﳵ���ܽ��
		 HashMap<Menu,Integer> map = getGoods();
    	 for (Menu key : map.keySet()) {  
    		  
    		    Integer value = map.get(key);  
    		  
    		    System.out.println("Key = " + key + ", Value = " + value);  
    		  
    		}  
		return true;
		
	}
	
	//ͳ�ƹ��ﳵ���ܽ��
	public double calTotalPrice()
	{
		double sum = 0.0;
		Set<Menu> keys = goods.keySet(); //��ü�ļ���
		Iterator<Menu> it = keys.iterator(); //��õ��������
	    while(it.hasNext())
	    {
	    	Menu i = it.next();
	    	sum += i.getMprice()* goods.get(i);
	    }
	    this.setTotalPrice(sum); //���ù��ﳵ���ܽ��
	    return this.getTotalPrice();
	}
	
	
}
