package com.qlw.service;

import java.util.List;

import com.qlw.bean.User;
import com.qlw.util.Page;

public interface UserService {

	/**
	 * 验证用户名和密码
	 * 
	 * @param username
	 *            用户名
	 * @param password
	 *            密码
	 * @return true 通过验证 false 不通过验证
	 */
	public boolean CheckUserAndPasswd(String username, String password);
	/**
	 * 添加用户
	 * @param user
	 */
	public void addUser(User user);
	
	/**
	 * 查找全部用户
	 * @return
	 */
	public List<User> findAll();
	
	/**
	 * 根据id查找用户
	 * @return
	 */
	public User findByid(String id);
	
	/**
	 * 修改用户信息
	 */
	public void updateUser(User user);
	
	/**
	 * 删除用户信息
	 * @param id
	 */
	public void delete(String id);
	
	/**
	 * 分页查找
	 * @param currentPageNo
	 * @return
	 */
	public Page<User> findPage(int currentPageNo);


}
