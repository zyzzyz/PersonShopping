package com.qlw.service.impl;

import java.util.List;

import com.qlw.bean.User;
import com.qlw.dao.UserDao;
import com.qlw.dao.impl.UserDaoImpl;
import com.qlw.service.UserService;
import com.qlw.util.Page;

public class UserServiceImpl implements UserService {
	
	public UserDao userdao = new UserDaoImpl();
	
	public boolean CheckUserAndPasswd(String username, String password) {
		
		return userdao.CheckUserAndPasswd( username,password);
	}

	public void addUser(User user) {
		userdao.addUser(user);
	}

	public List<User> findAll() {
		
		return userdao.findAll();
	}

	public User findByid(String id) {
		
		return userdao.findByid(id);
	}

	public void updateUser(User user) {
		
		userdao.updateUser(user);
	}

	public void delete(String id) {

        userdao.delete(id);
	}

	public Page<User> findPage(int currentPageNo) {
		
		return userdao.findPage(currentPageNo);
	}

}
