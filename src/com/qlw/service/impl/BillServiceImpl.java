package com.qlw.service.impl;

import java.util.List;

import com.qlw.bean.Bill;
import com.qlw.bean.Cart;
import com.qlw.bean.Menu;
import com.qlw.dao.BillDao;
import com.qlw.dao.impl.BillDaoImpl;
import com.qlw.service.BillService;
import com.qlw.util.Page;

public class BillServiceImpl implements BillService {

	private BillDao billDao=new BillDaoImpl();
	
	public void addBill(Cart cart)
	{
		billDao.addBill(cart);
	}
	public List<Bill> findAll() {
		
		return billDao.findAll();
	}

	public Bill findBybid(String bid) {
		
		return billDao.findBybid(bid);
	}

	public void delete(String bid) {
		billDao.delete(bid);

	}

	public Page<Bill> findPage(int currentPageNo) {
		
		return billDao.findPage(currentPageNo);
	}

}
