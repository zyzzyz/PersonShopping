package com.qlw.service.impl;

import java.util.List;

import com.qlw.bean.Menu;
import com.qlw.dao.MenuDao;
import com.qlw.dao.impl.MenuDaoImpl;
import com.qlw.service.MenuService;
import com.qlw.util.Page;

public class MenuServiceImpl implements MenuService {

	private MenuDao menudao = new MenuDaoImpl();
	public void addMenu(Menu menu) {
		menudao.addMenu(menu);

	}

	public List<Menu> findAll() {
		
		return menudao.findAll();
	}

	public Menu findByid(String id) {
		
		return menudao.findByid(id);
	}

	public void delete(String id) {
		menudao.delete(id);

	}

	public void updateMenu(Menu menu) {
	menudao.updateMenu(menu);

	}

	public Page<Menu> findPage(int currentPageNo) {
		
		return menudao.findPage(currentPageNo);
	}
	
	//����mname��ѯ����
	public boolean findBymname(String mname){
		
		return menudao.findBymname(mname);
	}

}
