package com.qlw.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Database {
	
	//数据库驱动
	
	private final static String DRIVER = "com.mysql.jdbc.Driver";

	/**
     * 数据库访问URL
     */
    private final static String URL="jdbc:mysql://127.0.0.1:3306/personShop";
    /**
     * 数据库访问用户名
     */
    private final static String  USERNAME = "root";
     /**
      * 数据库访问口令
      */
    private final static String  PASSWORD = "";
    /**
     * 获取数据库连接
     */
    public static Connection getconn() throws ClassNotFoundException{
   	      Connection conn = null;
   	      try{
   	    	  //加载数据库驱动
   	    	  Class.forName(DRIVER);
   	    	  //得到数据库连接
   	    	  conn = DriverManager.getConnection(URL,USERNAME,PASSWORD);
   	      }	catch(SQLException e){
   	    	  e.printStackTrace();
   	    	  
   	      }
   	      return conn;
    }
    /**
     * 创建PreparedStatement
     */
    public static PreparedStatement createpreStatement(Connection conn,String sql){
   	 PreparedStatement ps = null;
   	 try{
   		 ps = conn.prepareStatement(sql);
   	 }catch(Exception e){
   		 e.printStackTrace();
   	 }
   	 return ps;
    }
    
    /**
     * 预编译SQL编译(执行操作)
     */
    public static ResultSet executePreQuery(PreparedStatement ps,String sql){
   	 ResultSet rs =null;
   	 try{
   		 rs = ps.executeQuery(sql);
   		 
   	 }catch(SQLException e){
   		 e.printStackTrace();
   	 }
   	 
   	 return rs;
    }
    /**
     * 关闭连接释放资源
     */
    public static void closeStamentAndconn(ResultSet rs,PreparedStatement ps,Connection conn){
   	 
   	 try{
   		 if(rs != null){
   			 rs.close();
   			 rs = null;
   			 
   		 }
   		 if(ps != null)
   		 {
   			 ps.close();
   			 ps = null;
   		 }
   		 if(conn != null)
   		 {
   			 conn.close();
   			 conn = null;
   		 }
   	 }catch(SQLException e){
   		 e.printStackTrace();
   	 }
    }
}


