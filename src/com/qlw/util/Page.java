package com.qlw.util;

import java.util.List;

public class Page<T> {

	private int pageNo;//当前页
	
	private List<T> list;
	
	private int pageSize = 5;//内容的大小
	
	private long totalCount;//总共多少条记录
    
	public Page(int pageNo){
		super();
		this.pageNo = pageNo;
	}
	
	public int getPageNo(){
		if(pageNo < 0){
			pageNo = 1;
		}
		
		if(pageNo > getPageItemNumer()){
			pageNo = getPageItemNumer();
	}
	    return pageNo;
	}
	
     public int getPageSize(){
    	 return pageSize;
     }
     
     public List<T> getList(){
    	 return list;
    	 
     }
     
     public void setList(List<T> list){
    	 this.list = list;
     }
     
     public int getPageItemNumer(){
    	 return (int)(totalCount % pageSize == 0 ? totalCount / pageSize : ((totalCount / pageSize)+1));
     }
     
     public long getTotalCount(){
    	 return totalCount;
    	 
     }
     
     public void setTotalCount(long totalCount){
    	 this.totalCount = totalCount;
     }
     
     public boolean isHasNext(){
    	 if(getPageNo() < getPageItemNumer()){
    		 return true;
    	 }
    	 return false;
     }
     
     public boolean isHasPrev(){
    	 if(getPageNo() > 1){
    		 return true;
    		 
    	 }
    	 return false;
     }
     
     public int getNextPage() {
    	 if(isHasNext()){
    		 return getPageNo() + 1;
    	 }
    	 
    	 return getPageNo();
     }
     
     public int getPrevPage(){
    	 if(isHasPrev()){
    		return getPageNo() - 1; 
    	 }
    	 return getPageNo();
     }
}

