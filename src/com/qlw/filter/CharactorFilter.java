package com.qlw.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class CharactorFilter implements Filter {
	
	String encoding = null;//字符编码


	public void destroy() {
		encoding=null;
	}

	public void doFilter(ServletRequest request, ServletResponse reponse,
			FilterChain chain) throws IOException, ServletException {
		if(encoding!=null){//判断字符编码是否为空
			request.setCharacterEncoding(encoding);//设置request的编码格式
			reponse.setContentType("text/html;charset="+encoding);//设置reponse字符编码
		}
            chain.doFilter(request,reponse);//传递给下一个过滤器
	}

	public void init(FilterConfig filterConfig) throws ServletException {//FilterConfig接口用于获取过滤器中的配置信息
		encoding=filterConfig.getInitParameter("encoding");//获取初始化参数

	}

}
