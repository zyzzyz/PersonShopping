package com.qlw.dao;

import java.util.List;

import com.qlw.bean.Menu;
import com.qlw.bean.User;
import com.qlw.util.Page;

public interface MenuDao {
	
	//添加菜单
	public void addMenu(Menu menu);
	
	//查找全部菜单
    public List<Menu> findAll();
	

     // 根据mid 查找菜单
	public Menu findByid(String id);
	
	//删除菜单
	public void delete(String id);
	
	//更新菜单
	public void updateMenu(Menu menu);
	
	//分页查找
	public Page<Menu> findPage(int currentPageNo);
	
	//根据mname查询菜名
	public boolean findBymname(String mname);

}
