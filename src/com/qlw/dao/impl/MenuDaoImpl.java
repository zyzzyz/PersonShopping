package com.qlw.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.qlw.bean.Menu;
import com.qlw.dao.MenuDao;
import com.qlw.util.Database;
import com.qlw.util.Page;

public class MenuDaoImpl implements MenuDao {

	public void addMenu(Menu menu) {
		
		Connection conn = null;
		PreparedStatement ps = null;
		try{
			conn = Database.getconn();//获取数据库连接
		}catch(Exception e){
			
			throw new RuntimeException("get connection error!!");
		}
		
		try{
			
			String sql="Insert into menu(mname,mprice)value(?,?) ";
			ps = Database.createpreStatement(conn,sql);
			
			System.out.println(menu.getMprice());
			ps.setString(1,menu.getMname());
			ps.setFloat(2, menu.getMprice());
			
			ps.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		} finally {
			Database.closeStamentAndconn(null, ps, conn);
		}
	}
	
	public List<Menu> findAll() {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		List<Menu> listmenu = new ArrayList<Menu>();
		
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			String sql = "select * from menu order by id asc";
			ps = Database.createpreStatement(conn, sql);
			rs = ps.executeQuery();
			
			while(rs.next()){
				Menu menu = new Menu();
				menu.setId(rs.getInt("id"));
				menu.setMname(rs.getString("mname"));
				menu.setMprice(rs.getFloat("mprice"));
				listmenu.add(menu);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			Database.closeStamentAndconn(rs, ps, conn);
		}
		return listmenu;
	}

	public Menu findByid(String id) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			String sql = "select * from menu where id='"+id+"'";
			ps = Database.createpreStatement(conn, sql);
			rs = ps.executeQuery();
			
			while(rs.next()){
				Menu menu = new Menu();
				menu.setId(rs.getInt("id"));
				menu.setMname(rs.getString("mname"));
				menu.setMprice(rs.getFloat("mprice"));
				
				return menu;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			Database.closeStamentAndconn(rs, ps, conn);
		}
		return null;
	}

	public void delete(String id) {
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			String sql = "delete from menu where id='"+id+"'";
			ps = Database.createpreStatement(conn, sql);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Database.closeStamentAndconn(null, ps, conn);
		}
		

	}

	public void updateMenu(Menu menu) {
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			String sql = "update menu set  mname='"+menu.getMname()+"', mprice='"+menu.getMprice()+"' where id='"+menu.getId()+"'";
			
			ps = Database.createpreStatement(conn, sql);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Database.closeStamentAndconn(null, ps, conn);
		}
		
	}

	public Page<Menu> findPage(int currentPageNo) {
		Page<Menu> page = new Page<Menu>(currentPageNo);
		int totalCount = 0;
		try {
			totalCount = getTotalPageNumber();
		} catch (SQLException e1) {
			
			e1.printStackTrace();
		}
		page.setTotalCount(totalCount);
		List<Menu> menus = null;
		try {
			menus = getPageList(currentPageNo, page.getPageSize());
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		page.setList(menus);
		return page;
	}
	
	private int getTotalPageNumber() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int totalCount = 0;
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e1) {
			
			e1.printStackTrace();
		}
			
		try {
			String sql = "select count(*) from menu";
			ps = Database.createpreStatement(conn, sql);
			rs = ps.executeQuery();
				
			while(rs.next()){
				totalCount = Integer.parseInt(rs.getString(1));
			}
				
				
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			Database.closeStamentAndconn(rs, ps, conn);
		}
		return totalCount;
}


private List<Menu> getPageList(int currentPageNo,int pageSize) throws SQLException {
	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	List<Menu> listmenu = new ArrayList<Menu>();
	
	try {
		conn = Database.getconn();
	} catch (ClassNotFoundException e1) {
	
		e1.printStackTrace();
	}
		
	try {
		String sql = "select * from menu limit ?,? ";
		ps = Database.createpreStatement(conn, sql);
		
		ps.setInt(1, (currentPageNo-1)*pageSize);
		ps.setInt(2, pageSize);
		rs = ps.executeQuery();
			
		while(rs.next()){
			
			Menu menu = new Menu();
			menu.setId(rs.getInt("id"));
			menu.setMname(rs.getString("mname"));
			menu.setMprice(rs.getFloat("mprice"));
			listmenu.add(menu);
		}
			
	} catch (SQLException e) {
		e.printStackTrace();
	}finally {
		Database.closeStamentAndconn(rs, ps, conn);
	}
	return listmenu;
}


public boolean findBymname(String mname) {
	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	boolean b=false;
	try {
		conn = Database.getconn();
	} catch (ClassNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	try {
		String sql = "select * from menu where mname='"+mname+"'";
		ps = Database.createpreStatement(conn, sql);
		rs = ps.executeQuery();
		
		while(rs.next()){
			
			b=true;
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}finally {
		Database.closeStamentAndconn(rs, ps, conn);
	}
	return b;
}
}
