package com.qlw.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.qlw.bean.Bill;
import com.qlw.bean.Cart;
import com.qlw.bean.Menu;

import com.qlw.dao.BillDao;
import com.qlw.util.Database;
import com.qlw.util.Page;


public class BillDaoImpl implements BillDao {
	
	//添加账单
		public void addBill(Cart cart){
			//获取商品数量和信息
			HashMap<Menu, Integer> goods=cart.getGoods();
			//Menu menu=goods
			Connection conn = null;
			PreparedStatement ps = null;
			
			try{
				conn = Database.getconn();//获取数据库连接
			}catch(Exception e){
				
				throw new RuntimeException("get connection error!!");
			}
			
			try{
				 
			         Set<Menu> items = goods.keySet();
			         Iterator<Menu> it = items.iterator();
			         
			         while(it.hasNext())
			         {
			            Menu menu = it.next();
			            Integer integer = (Integer)goods.get(menu);
				
				String sql="Insert into bill(mid,mname,mprice,number)value(?,?,?,?) ";
				ps = Database.createpreStatement(conn,sql);
				
				ps.setInt(1,menu.getId());
				ps.setString(2,menu.getMname());
				ps.setFloat(3, menu.getMprice());
				ps.setInt(4, integer);
				
				System.out.println();
				ps.executeUpdate();
				}
			}catch(SQLException e) {
				e.printStackTrace();
			} finally {
				Database.closeStamentAndconn(null, ps, conn);
			
			
			}
		}

	public List<Bill> findAll() {
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		List<Bill> listBill = new ArrayList<Bill>();
		
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			String sql = "select * from bill order by id asc";
			ps = Database.createpreStatement(conn, sql);
			rs = ps.executeQuery();
			
			while(rs.next()){
				Bill bill = new Bill();
				bill.setBid(rs.getInt("bid"));
				bill.setMname(rs.getString("mname"));
				bill.setMprice(rs.getFloat("mprice"));
				bill.setNumber(rs.getFloat("number"));
				listBill.add(bill);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			Database.closeStamentAndconn(rs, ps, conn);
		}
		return listBill;
	}

	public Bill findBybid(String bid) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			String sql = "select * from bill where bid='"+bid+"'";
			ps = Database.createpreStatement(conn, sql);
			rs = ps.executeQuery();
			
			while(rs.next()){
				Bill bill = new Bill();
				bill.setBid(rs.getInt("id"));
				bill.setMname(rs.getString("mname"));
				bill.setMprice(rs.getFloat("mprice"));
				bill.setNumber(rs.getFloat("number"));
				return bill;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			Database.closeStamentAndconn(rs, ps, conn);
		}
		return null;
	}

	public void delete(String bid) {
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			String sql = "delete from bill where bid='"+bid+"'";
			ps = Database.createpreStatement(conn, sql);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Database.closeStamentAndconn(null, ps, conn);
		}
		


	}

	public Page<Bill> findPage(int currentPageNo) {
		Page<Bill> page = new Page<Bill>(currentPageNo);
		int totalCount = 0;
		try {
			totalCount = getTotalPageNumber();
		} catch (SQLException e1) {
			
			e1.printStackTrace();
		}
		page.setTotalCount(totalCount);
		List<Bill> Bills = null;
		try {
			Bills = getPageList(currentPageNo, page.getPageSize());
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		page.setList(Bills);
		return page;
	}
	
	private int getTotalPageNumber() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int totalCount = 0;
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e1) {
			
			e1.printStackTrace();
		}
			
		try {
			String sql = "select count(*) from bill";
			ps = Database.createpreStatement(conn, sql);
			rs = ps.executeQuery();
				
			while(rs.next()){
				totalCount = Integer.parseInt(rs.getString(1));
			}
				
				
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			Database.closeStamentAndconn(rs, ps, conn);
		}
		return totalCount;
}


private List<Bill> getPageList(int currentPageNo,int pageSize) throws SQLException {
	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	List<Bill> listBill = new ArrayList<Bill>();
	
	try {
		conn = Database.getconn();
	} catch (ClassNotFoundException e1) {
	
		e1.printStackTrace();
	}
		
	try {
		String sql = "select * from bill limit ?,? ";
		ps = Database.createpreStatement(conn, sql);
		
		ps.setInt(1, (currentPageNo-1)*pageSize);
		ps.setInt(2, pageSize);
		rs = ps.executeQuery();
			
		while(rs.next()){
			
			Bill bill = new Bill();
			bill.setBid(rs.getInt("bid"));
			bill.setMname(rs.getString("mname"));
			bill.setMprice(rs.getFloat("mprice"));
			bill.setNumber(rs.getFloat("number"));
			listBill.add(bill);
		}
			
	} catch (SQLException e) {
		e.printStackTrace();
	}finally {
		Database.closeStamentAndconn(rs, ps, conn);
	}
	return listBill;
}


}


