package com.qlw.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.qlw.bean.User;
import com.qlw.dao.UserDao;
import com.qlw.util.Page;
import com.qlw.util.Database;

public class UserDaoImpl implements UserDao {

	public boolean CheckUserAndPasswd(String username, String password) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			//获取连接
			conn = Database.getconn();
			
			String sql = "select * from user where username='"+username+"' and  password='"+password+"'";
			
			//创建PreparedStatement
			ps = Database.createpreStatement(conn, sql);
			
			//执行操作获取结果集
			rs = Database.executePreQuery(ps, sql);
			if(rs.next()){
				return true;
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			Database.closeStamentAndconn(rs, ps, conn);
		}
		return false;
	}

	public void addUser(User user) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			
			String sql = "Insert into user(username,password) values(?,?)";
			ps = Database.createpreStatement(conn, sql);
			
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Database.closeStamentAndconn(null, ps, conn);
		}

	}

	public List<User> findAll() {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		List<User> listuser = new ArrayList<User>();
		
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			String sql = "select * from user order by id asc";
			ps = Database.createpreStatement(conn, sql);
			rs = ps.executeQuery();
			
			while(rs.next()){
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				listuser.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			Database.closeStamentAndconn(rs, ps, conn);
		}
		return listuser;
	}

	public User findByid(String id) {

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			String sql = "select * from user where id='"+id+"'";
			ps = Database.createpreStatement(conn, sql);
			rs = ps.executeQuery();
			
			while(rs.next()){
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				
				return user;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			Database.closeStamentAndconn(rs, ps, conn);
		}
		return null;
	}

	public void updateUser(User user) {
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			String sql = "update user set  username='"+user.getUsername()+"', password='"+user.getPassword()+"' where id='"+user.getId()+"'";
			
			ps = Database.createpreStatement(conn, sql);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Database.closeStamentAndconn(null, ps, conn);
		}
		
	}

	public void delete(String id) {
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			String sql = "delete from user where id='"+id+"'";
			ps = Database.createpreStatement(conn, sql);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Database.closeStamentAndconn(null, ps, conn);
		}
		
	}

	public Page<User> findPage(int currentPageNo) {
		Page<User> page = new Page<User>(currentPageNo);
		int totalCount = 0;
		try {
			totalCount = getTotalPageNumber();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		page.setTotalCount(totalCount);
		List<User> users = null;
		try {
			users = getPageList(currentPageNo, page.getPageSize());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		page.setList(users);
		return page;
	}

	private int getTotalPageNumber() throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int totalCount = 0;
		try {
			conn = Database.getconn();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
			
		try {
			String sql = "select count(*) from user";
			ps = Database.createpreStatement(conn, sql);
			rs = ps.executeQuery();
				
			while(rs.next()){
				totalCount = Integer.parseInt(rs.getString(1));
			}
				
				
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			Database.closeStamentAndconn(rs, ps, conn);
		}
		return totalCount;
}


private List<User> getPageList(int currentPageNo,int pageSize) throws SQLException {
	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	List<User> listuser = new ArrayList<User>();
	
	try {
		conn = Database.getconn();
	} catch (ClassNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
		
	try {
		String sql = "select * from user limit ?,? ";
		ps = Database.createpreStatement(conn, sql);
		
		ps.setInt(1, (currentPageNo-1)*pageSize);
		ps.setInt(2, pageSize);
		rs = ps.executeQuery();
			
		while(rs.next()){
			
			User user = new User();
			user.setId(rs.getInt("id"));
			user.setUsername(rs.getString("username"));
			user.setPassword(rs.getString("password"));
			listuser.add(user);
		}
			
	} catch (SQLException e) {
		e.printStackTrace();
	}finally {
		Database.closeStamentAndconn(rs, ps, conn);
	}
	return listuser;
}

}
