package com.qlw.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.qlw.bean.Menu;
import com.qlw.util.Database;



//商品的业务逻辑类
public class cartDao {

	// 获得所有的商品信息
	public ArrayList<Menu> getAllMenu() {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ArrayList<Menu> list = new ArrayList<Menu>(); // 商品集合
		try {
			conn =Database.getconn();
			String sql = "select * from menu;"; // SQL语句
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();
			while (rs.next()) {
				Menu item = new Menu();
				item.setId(rs.getInt("id"));
				item.setMname(rs.getString("mname"));
			    item.setMprice(rs.getFloat("mprice"));
				
				list.add(item);// 把一个商品加入集合
			}
			return list; // 返回集合。
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		} finally {
			// 释放数据集对象
			if (rs != null) {
				try {
					rs.close();
					rs = null;
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			// 释放语句对象
			if (stmt != null) {
				try {
					stmt.close();
					stmt = null;
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}

	}

	// 根据商品编号获得商品资料
	public Menu getMenuById(String id) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = Database.getconn();
			String sql = "select * from menu where id='"+id+"'"; // SQL语句
		//	stmt = conn.prepareStatement(sql);
			//stmt.setInt(1,id);
			ps = Database.createpreStatement(conn, sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				Menu item = new Menu();
				item.setId(rs.getInt("id"));
				item.setMname(rs.getString("mname"));
				
				item.setMprice(rs.getFloat("mprice"));
				
				return item;
			} else {
				return null;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		} finally {
			// 释放数据集对象
			if (rs != null) {
				try {
					rs.close();
					rs = null;
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			// 释放语句对象
			if (ps != null) {
				try {
					ps.close();
					ps = null;
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

		}
	}

	
	

}
