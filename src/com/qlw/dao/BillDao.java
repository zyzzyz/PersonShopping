package com.qlw.dao;

import java.util.List;


import com.qlw.bean.Bill;
import com.qlw.bean.Cart;
import com.qlw.util.Page;

public interface BillDao {
	
	//添加账单
	public void addBill(Cart cart);
	//查找全部帐单
    public List<Bill> findAll();
	

     // 根据mid 查找帐单
	public Bill findBybid(String bid);
	
	//删除帐单
	public void delete(String bid);
	
	//分页查找
		public Page<Bill> findPage(int currentPageNo);

}
