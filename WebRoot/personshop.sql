/*
Navicat MySQL Data Transfer

Source Server         : 123456
Source Server Version : 50540
Source Host           : 127.0.0.1:3306
Source Database       : personshop

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2017-06-03 00:08:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bill
-- ----------------------------
DROP TABLE IF EXISTS `bill`;
CREATE TABLE `bill` (
  `bid` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `mprice` double NOT NULL,
  `number` double NOT NULL,
  PRIMARY KEY (`bid`)
) ENGINE=InnoDB AUTO_INCREMENT=2016102195 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bill
-- ----------------------------
INSERT INTO `bill` VALUES ('2016102192', '2035', '闹钟', '20', '1');
INSERT INTO `bill` VALUES ('2016102193', '2035', '闹钟', '20', '1');
INSERT INTO `bill` VALUES ('2016102194', '2035', '闹钟', '20', '1');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mname` varchar(255) NOT NULL,
  `mprice` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2037 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('2035', '闹钟', '20');
INSERT INTO `menu` VALUES ('2036', '水瓶', '30');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('24', 'admin', '123');
INSERT INTO `user` VALUES ('25', '', '');
INSERT INTO `user` VALUES ('26', 'admin', '693');
INSERT INTO `user` VALUES ('27', 'admin', '693');
INSERT INTO `user` VALUES ('28', 'admin', '693');
