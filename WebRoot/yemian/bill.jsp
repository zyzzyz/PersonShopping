<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>账单信息</title>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script language="javascript">
	    function delete1() {
	        if (!confirm("确认要删除？")) {
	            window.event.returnValue = false;
	        }
	    }
 </script>

</head>
<style type="text/css">

th {
      text-align:center; /*设置水平居中*/
      vertical-align:middle;/*设置垂直居中*/
}

td {
      text-align:center; /*设置水平居中*/
      vertical-align:middle;/*设置垂直居中*/
     
}
.kuoh {
height: 30px;
padding: 5px 10px;
font-size: 12px;
line-height: 0.5;
border-radius: 3px;
}

</style>
<body>

<div class="table-responsive">
   <table class="table table-bordered">
   <thead>
     <tr>
       <th>购买个人物品编号</th>
       <th>购买个人物品名称</th>
       <th>购买个人物品单价</th>
       <th>购买数量</th>
       <th>操作</th>
     </tr>
   </thead>
   <tbody>
   <c:forEach items="${listBill.list}" var="bill">
     <tr>
        <td align = "center" id="id">${bill.bid}</td>
        <td align = "center" id="mname"> ${bill.mname}</td>
        <td align = "center" id="mprice"> ${bill.mprice}</td>
        <td align = "center" id="number"> ${bill.number}</td>    
        <td align = "center" > <a href="<%=path%>/billServlet?method=delete&id=${bill.bid} " onclick="delete1();">删除</a></td>
      </tr>
     </c:forEach>
   </tbody> 
 </table>
 <ul class="pager">
  <td > 共 ${listBill.pageItemNumer}页&nbsp; 
                   当前第${listBill.pageNo}页&nbsp;共${listbill.totalCount}条记录
        &nbsp; 
  <c:if test="${listBill.hasPrev}">
   <a href="<%=path%>/billServlet?method=page&pageNo=1">首页</a>
  <li>   <a href="<%=path%>/billServlet?method=page&pageNo=${listBill.prevPage}">上一页</a> </li>
   </c:if>
   
   <c:if test="${listBill.hasNext}"> 
  <li> <a href="<%=path%>/billServlet?method=page&pageNo=${listBill.nextPage}">下一页</a></li>
   <a href="<%=path%>/billServlet?method=page&pageNo=${listBill.pageItemNumer}">末页</a>
           </c:if>
           
 
  </td> 
</ul>
  </div> 
   

</body>
</html>