<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>菜单管理</title>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

</head>
<script type="text/javascript">
	    function delete1() {
	        if (!confirm("确认要删除？")) {
	            window.event.returnValue = false;
	        }
	    }
	    
	     
 </script>
<style type="text/css">

th {
      text-align:center; /*设置水平居中*/
      vertical-align:middle;/*设置垂直居中*/
}

td {
      text-align:center; /*设置水平居中*/
      vertical-align:middle;/*设置垂直居中*/
}
.kuoh {
height: 30px;
padding: 5px 10px;
font-size: 12px;
line-height: 0.5;
border-radius: 3px;
}

</style>
<body>

<div class="table-responsive">
<form action="<%=path %>/menuServlet?method=page" method="post">
   <table class="table table-bordered">
   <thead>
     <tr>
       <th>个人物品编号</th>
       <th>个人物品名称</th>
       <th>个人物品单价</th>
        <th>操作</th>
     </tr>
   </thead>
   <tbody>
   <c:forEach items="${listmenu.list}" var="menu">
     <tr>
        <td id="id">${menu.id}</td>
        <td id="mname"> ${menu.mname}</td>
        <td id="mprice"> ${menu.mprice}</td>
        <td ><a href="<%=path%>/menuServlet?method=find&id=${menu.id}">修改</a>
         <a href="<%=path%>/menuServlet?method=delete&id=${menu.id}" onclick="delete1()">删除</a>
        </td>
        
      </tr>
     </c:forEach>
   </tbody> 
 </table>
 </form>
 <ul class="pager">
  <td > 共 ${listmenu.pageItemNumer}页&nbsp; 
                   当前第${listmenu.pageNo}页&nbsp;共${listmenu.totalCount}条记录
        &nbsp; 
  <c:if test="${listmenu.hasPrev}">
   <a href="<%=path%>/menuServlet?method=page&pageNo=1">首页</a>
  <li>   <a href="<%=path%>/menuServlet?method=page&pageNo=${listmenu.prevPage}">上一页</a> </li>
   </c:if>
   
   <c:if test="${listmenu.hasNext}"> 
  <li> <a href="<%=path%>/menuServlet?method=page&pageNo=${listmenu.nextPage}">下一页</a></li>
   <a href="<%=path%>/menuServlet?method=page&pageNo=${listmenu.pageItemNumer}">末页</a>
           </c:if>
           
 
  </td> 
</ul>
  </div> 


</body>
</html>