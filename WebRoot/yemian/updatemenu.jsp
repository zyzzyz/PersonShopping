<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>菜单管理</title>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<style type="text/css">

th {
      text-align:center; /*设置水平居中*/
      vertical-align:middle;/*设置垂直居中*/
}

td {
      text-align:center; /*设置水平居中*/
      vertical-align:middle;/*设置垂直居中*/
     
}
.kuoh {
height: 30px;
padding: 5px 10px;
font-size: 12px;
line-height: 0.5;
border-radius: 3px;
}

</style>
<body>

<div class="table-responsive">
	<form action="<%=path%>/menuServlet?method=update" method="post">
   <table class="table table-bordered">
   <tr>
  				<td>菜单号：</td>
  				<td>
  					<input type="text" id="id" name="id" value="${menu.id}" disabled="disabled"/>
  					<input type="hidden" id="id" name="id" value="${menu.id}" />
  				</td>
  			</tr>
			<tr>
  				<td>菜单名：</td>
  				<td><input type="text" id="mname" name="mname" value="${menu.mname}"/></td>
  			</tr>
  			<tr>
  				<td>单价：</td>
  				<td><input type="text" id="mprice" name="mprice" value="${menu.mprice}"/></td>
  			</tr>
  			<tr>
  				<td>
  					<input type="submit" value="修改" />
  				</td>
  				<td>
  					<input type="reset" value="重置"/>
  				</td>
  			</tr>
		</table>
	</form>
</div>

</body>
</html>