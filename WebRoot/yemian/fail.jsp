<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.qlw.bean.Cart" %>
<%@ page import="com.qlw.bean.Menu" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>菜单管理</title>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

</head>

<style type="text/css">
th {
      text-align:center; /*设置水平居中*/
      vertical-align:middle;/*设置垂直居中*/
}

td {
      text-align:center; /*设置水平居中*/
      vertical-align:middle;/*设置垂直居中*/
     
}
.kuoh {
height: 30px;
padding: 5px 10px;
font-size: 12px;
line-height: 0.5;
border-radius: 3px;
}

</style>
<body>

<h1>购物清单</h1>
  
   <hr> 
<div class="table-responsive">
<form  action="<%=path %>/cartServlet?action=empty" method="post" name = "form">
	 <table class="table table-bordered">	
	<tr>
       
       <th>购买个人物品名称</th>
       <th>购买个人物品单价</th>
       <th>购买数量</th>
       <th>购买个人物品总价</th>
      
     </tr> 

   <% 
				   //首先判断session中是否有购物车对象
				   if(request.getSession().getAttribute("cart")!=null)
				   {
				%>
				
					 

				<!-- 循环的开始 -->
				     <% 
				         Cart cart = (Cart)request.getSession().getAttribute("cart");
				         HashMap<Menu,Integer> goods = cart.getGoods();
				         Set<Menu> items = goods.keySet();
		
				         Iterator<Menu> it = items.iterator();
	
				         
				         while(it.hasNext())
				         {
				            Menu i = it.next();
				     %> 
  
   
  
     <tr>
		
		<th class="mname"><%=i.getMname() %></th>
	
		
		<th class="number"><%=i.getMprice() %></th>
	
				
		<th class="number">
                     	<%=goods.get(i)%>					
					</th> 
		<th class="price" id="price_id_1">
						<span><%=i.getMprice()*goods.get(i) %></span>
						<input type="hidden" value="" />
					</th>
	

	</tr>
	<% 
				         }
				     %>

				
	<tr>
	  
	   <th></th>
	    <th>总计:</th>
	   <th></th>
	   <th><%=cart.getTotalPrice() %>￥</th>
	</tr>
	<% 
				         }
				     %>
	<tr>
	    
	    <th></th>
	    <th>实收金额</th>
	    <th></th>
	    <th>${money}</th>
	</tr>
	<tr>
	   
	    <th></th>
	     <th>找零</th>
	    <th></th>
	    <th>${zero}</th>
	</tr>
	  
				
   
 </table>
 
  <div>
		     <button   type="submit">确认</button>
</div>

		</form>
  </div> 


</body>
</html>