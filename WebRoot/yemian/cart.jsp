<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.qlw.bean.Cart" %>
<%@ page import="com.qlw.bean.Menu" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>菜单管理</title>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

</head>
<style type="text/css">

th {
      text-align:center; /*设置水平居中*/
      vertical-align:middle;/*设置垂直居中*/
}

td {
      text-align:center; /*设置水平居中*/
      vertical-align:middle;/*设置垂直居中*/
     
}
.kuoh {
height: 30px;
padding: 5px 10px;
font-size: 12px;
line-height: 0.5;
border-radius: 3px;
}

</style>
<body>

<h1>我的购物车</h1>
    <a href="<%=path%>/menuServlet?method=page1" class="left_menu1">个人物品列表</a>
     <a href="<%=path%>/cartServlet?action=empty1" class="left_menu1">清空购物车</a>
   <hr> 
<div class="table-responsive">
<form  action="<%=path %>//cartServlet?action=bill" method="post" name = "form">
   <table class="table table-bordered">
   <thead>
     <tr>
					<th>个人物品名称</th>
					<th>个人物品单价</th>
					<th>个人物品价格</th>
					<th>购买数量</th>
					<th>操作</th>
				</tr>
				<% 
				   //首先判断session中是否有购物车对象
				   if(request.getSession().getAttribute("cart")!=null)
				   {
				%>
				<!-- 循环的开始 -->
				     <% 
				         Cart cart = (Cart)request.getSession().getAttribute("cart");
				         HashMap<Menu,Integer> goods = cart.getGoods();
				         Set<Menu> items = goods.keySet();
				         Iterator<Menu> it = items.iterator();
				         
				         while(it.hasNext())
				         {
				            Menu i = it.next();
				     %> 
   </thead>
   <tbody>
   <tr name="products" id="product_id_1">
					<td class="mname"><%=i.getMname() %></td>
					<td class="number"><%=i.getMprice() %></td>
					<td class="price" id="price_id_1">
						<span><%=i.getMprice()*goods.get(i) %></span>
						<input type="hidden" value="" />
					</td>
					<td class="number">
                     	<%=goods.get(i)%>					
					</td>                        
                    <td class="delete">
					  <a href="<%=path%>/cartServlet?action=delete&id=<%=i.getId()%>" onclick="delcfm();">删除</a>					                  
					</td>
				</tr>
				<% 
				         }
				     %>
				<!--循环的结束-->
   </tbody> 
 </table>
 
  <div><span id="total">总计：<%=cart.getTotalPrice() %>￥</span></div>
			   <% 
			    }
			 %>
			实收金额:<input type="text" id="money" name="money" />
		     <button   type="submit">付款</button>
		</form>
  </div> 


</body>
</html>