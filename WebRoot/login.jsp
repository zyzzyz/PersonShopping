﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>个人物品交易系统</title>
        <link type="text/css" rel="stylesheet" media="all" href="styles/global.css" />
        <link type="text/css" rel="stylesheet" media="all" href="styles/global_color.css" /> 
    </head>
    <body class="index">
        <div class="login_box">
    <form id = "myform" name="myform" action="<%=path%>/loginServlet" method="post">
        
            <table>
                <tr>
                    <td class="login_info">账号：</td>
                    <td colspan="2"><input name="userName" type="text" class="width150" /></td>
                    <td class="login_error_info"></td>
                </tr>
                <tr>
                    <td class="login_info">密码：</td>
                    <td colspan="2"><input name="password" type="password" class="width150" /></td>
                    <td></td>
                </tr>
               
                <tr>
				<td></td>
                     <td class="login_button" colspan="2">
                     
                     <a href="javascript:document.getElementById('myform').submit();"> <img  id="img" src="images/login_btn.png" /></a> 
                    </td> 
					  </tr>
               
                <tr>
				<td></td>
                    <td class="login_button" colspan="2">
                     <a href="register.jsp"> <img  id="img" src="images/register.png" /></a> 
                    </td>    
                    <td></td>                
                </tr>
            </table>
            </form>
        </div>
    </body>
</html>
